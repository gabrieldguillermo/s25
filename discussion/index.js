
/*
	JSON Object
		- JSON stands for javascript Object notation
		- JSON is also used for other programming languages hence the name Javascript Object Notation


	Syntax:
		{
			"propertyA" : "valueA",
			"propertyB" : "valueB"
		}
*/

// JSON Object

/*{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
}*/

// "cities" = [
// 	{"city" : "Quezon City", "province" : "Metro Manila", "country" : "Philippines"},
// 	{"city" : "Manila", "province" : "Metro Manila", "country" : "Philippines"}
	
// ]

// JSON Methods
/*
	The JSON Object contains method for parsing and converting data into stringified JSON
*/

// Converting Data into Stringified JSON

let batchArr = [
	{batchName: "Batch 197"},
	{batchName: "Batch 198"}
]

console.log(batchArr)
batchArr = JSON.stringify(batchArr);
console.log(batchArr);

let userProfile = JSON.stringify({
	name : "John",
	age : 31 ,
	address : {
		city : "Manila",
		region : "Metro Manila",
		country : "Philippines"
	}
})

console.log(userProfile);


// user details
/*let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age= prompt("What is your age?");
let address = {
  city : prompt("Which city do you live?"),
	country : prompt("Which country does your city address belong to?")
};

let userData = JSON.stringify({
	firstName : firstName ,
	lastName : lastName ,
	age : age ,
	address : address
		
});
console.log(userData);*/



let stringifiedOblect = `{
	"name": "Ivy",
	"age" : "18",
	"address" : {
			"city" : "Tacloban City",
			"country" : "Philippines"
	}
}

` ;

console.log(stringifiedOblect);
console.log("Result from parse method");
console.log(JSON.parse(stringifiedOblect));